## Bus Station Application

Management and administration of automotive to make your dispatch process more efficient.

## System Requirements
* PHP ^7.1.3
* Node.js ^8.11.1
* Composer ^1.5.2

### How to use

Step 1. Install and configure Passport Server

* Go to folder /bus-station-api in your Terminal or Command Prompt
* Copy .env.example to .env and fill in your database credentials
* Run composer install
* Run php artisan key:generate
* Run php artisan migrate --seed
* Run php artisan passport:install
* Copy the CLIENT_SECRET of the client section with the Client ID 2 and save it


Step 2. Install and configure The Client

* Go to folder /bus-station-frontend in your Terminal or Command Prompt
* Run npm install
* Go to the file site.js inside of src/config folder
	* Set API_URL where the bus-station-api is hosted
	* Set the CLIENT_SECRET value from the generated key in the last step of the server configuration. Example => Mbt7HN09lO5h3xUU21sdLBqHVSHbS7JYwadGqHlE
* Run npm run dev

To consume services you need a client like postman, these are the routes:


- GET: http://patch/to/route/{method}
- GET: http://patch/to/route/{method}/{id}
- POST: http://patch/to/route/{method}
	- x-www-form-urlencoded
		- foo: x (string)
		- bar: x (string)
- PUT: http://patch/to/route/{method}/{id}
	- x-www-form-urlencoded
		- foor: x (string)
		- bar: x (string)
- DELETE: http://http://patch/to/route/{method}/delete

methods {
		customer/* {GET/POST/PUT/DELETE},
		oauth/token {POST},
		auth/register {POT},
	}

Example:

- GET: http://bus-station.io/api/terminal
- GET: http://bus-station.io/api/terminal/1
- POST: http://bus-station.io/api/terminal
	- x-www-form-urlencoded
		- name: Sura
		- description: Terminar Sura
		- active: 1
- PUT: http://bus-station.io/api/terminal/1
	- x-www-form-urlencoded
		- name: Sura
		- description: Terminar Sura
		- active: 1
- POST: http://bus-station.io/api/terminal/delete
		- x-www-form-urlencoded
		- remove: remove[0] = 7