<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class StationServiceProvider
 * @package App\Providers
 */
class StationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    /**
     * Package boot method
     */
    public function boot()
    {
        //
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerStation();
        $this->registerFacade();
        $this->registerBindings();
    }
    /**
     * Register the application bindings.
     *
     * @return void
     */
    public function registerStation()
    {
        //
    }
    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade()
    {
        //
    }
    /**
     * Register service provider bindings
     */
    public function registerBindings()
    {
        $this->app->bind(
            \App\Repositories\Terminal\TerminalContract::class,
            \App\Repositories\Terminal\EloquentTerminalRepository::class
        );

        $this->app->bind(
            \App\Repositories\Bus\BusContract::class,
            \App\Repositories\Bus\EloquentBusRepository::class
        );

        $this->app->bind(
            \App\Repositories\Route\RouteContract::class,
            \App\Repositories\Route\EloquentRouteRepository::class
        );

        $this->app->bind(
            \App\Repositories\Ticket\TicketContract::class,
            \App\Repositories\Ticket\EloquentTicketRepository::class
        );

        $this->app->bind(
            \App\Repositories\User\UserContract::class,
            \App\Repositories\User\EloquentUserRepository::class
        );
    }
}