<?php namespace App\Models\Bus;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bus
 * @package App\Models\Bus
 */
Class Bus extends Model
{
    /**
     * The database table used by the model.
     * @var mixed
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['name', 'driver_name', 'bus_model', 'seats', 'active'];

    /**
     * Bus constructor.
     */
    public function __construct()
    {
        $this->table = config('station.table_names.buses');
    }
}