<?php

namespace App\Models\Route\Traits\Relationship;

use App\Models\Terminal\Terminal;
use App\Models\Bus\Bus;

/**
 * Trait RouteRelationship
 * @package App\Models\Route\Traits\Relationship
 */
trait RouteRelationship
{
    /**
     * @return mixed
     */
    public function terminal_starts()
    {
        return $this->belongsTo(Terminal::class, 'terminal_start_id');
    }

    /**
     * @return mixed
     */
    public function terminal_ends()
    {
        return $this->belongsTo(Terminal::class, 'terminal_end_id');
    }

    /**
     * @return mixed
     */
    public function buses()
    {
        return $this->belongsTo(Bus::class, 'bus_id');
    }
}