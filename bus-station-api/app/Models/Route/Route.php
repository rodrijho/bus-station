<?php

namespace App\Models\Route;

use Illuminate\Database\Eloquent\Model;
use App\Models\Route\Traits\Relationship\RouteRelationship;

/**
 * Class Route
 * @package App\Models\Route
 */
class Route extends Model
{
    use RouteRelationship;

    /**
     * @var mixed
     */
    protected $table;

    /**
     * @var array
     */
    protected $fillable = ['bus_id', 'terminal_start_id', 'terminal_end_id', 'price', 'active'];

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->table = config('station.table_names.routes');
    }
}