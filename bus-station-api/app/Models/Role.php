<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @package App\Models
 * @property string $title
 */
class Role extends Model
{
    protected $fillable = ['title'];
}
