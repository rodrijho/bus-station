<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ticket\Traits\Relationship\TicketRelationship;

/**
 * Class Ticket
 * @package App\Models\Ticket
 */
class Ticket extends Model
{
    use TicketRelationship;

    /**
     * @var mixed
     */
    protected $table;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'route_id', 'code'];

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->table = config('station.table_names.tickets');
    }
}