<?php

namespace App\Models\Ticket\Traits\Relationship;

use App\Models\User;
use App\Models\Route\Route;

/**
 * Trait TicketRelationship
 * @package App\Models\Ticket\Traits\Relationship
 */
trait TicketRelationship
{
    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return mixed
     */
    public function routes()
    {
        return $this->belongsTo(Route::class, 'route_id');
    }
}