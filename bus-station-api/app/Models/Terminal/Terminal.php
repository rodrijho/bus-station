<?php

namespace App\Models\Terminal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Terminal
 * @package App\Models\Terminal
 */
Class Terminal extends Model
{
    /**
     * The database table used by the model.
     * @var mixed
     */
    protected $table;


    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['name', 'description', 'active'];

    /**
     * Terminal constructor.
     */
    public function __construct()
    {
        $this->table = config('station.table_names.terminals');
    }
}