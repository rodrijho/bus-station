<?php

namespace App\Repositories\Route;

use App\Models\Route\Route;

/**
 * Class EloquentRouteRepository
 * @package App\Repositories\Route
 */
class EloquentRouteRepository implements RouteContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRoutes($order_by = 'id', $sort = 'asc')
    {
        $routes = Route::orderBy($order_by, $sort)
        ->with(['buses', 'terminal_starts', 'terminal_ends'])
        ->get();

        $sanitizedRoutes = [];
        $newRoute = [];

        foreach( $routes as $route ){
            $newRoute = [
                'id'                => $route->id,
                'route_name'        => $route->route_name,
                'price'             => '$' . $route->price,
                'bus_name'          => $route->buses->name,
                'start_terminal'    => $route->terminal_starts->name,
                'end_terminal'      => $route->terminal_ends->name,
            ];

            array_push($sanitizedRoutes, $newRoute);
            $newRoute = [];
        }

        return response()->json([
            'success'   => 1,
            'info' => $sanitizedRoutes,
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRouteById($id)
    {
        $route = Route::find($id);

        if (! is_null($route)) {
            return response()->json([
                'success' => 1,
                'info' => [
                    'id'                => $route->id,
                    'route_name'        => $route->route_name,
                    'bus_id'            => $route->bus_id,
                    'bus'               => $route->buses->name,
                    'terminal_start_id' => $route->terminal_start_id,
                    'terminal_start'    => $route->terminal_starts->name,
                    'terminal_end_id'   => $route->terminal_end_id,
                    'terminal_end'      => $route->terminal_ends->name,
                    'price'             => $route->price,
                    'active'            => $route->active,
                    'created_at'        => $route->created_at->format('Y-m-d H:i:s'),
                    'updated_at'        => $route->updated_at->format('Y-m-d H:i:s'),
                ],
            ], 200);
        } else {
            return response()->json([
                'message' => 'That route does not exist.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($request)
    {
        $route = $this->createRouteStub($request);

        if ($route->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Route successfully created.',
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem creating this route. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $id
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $request)
    {
        $route = Route::find($id);

        $route->route_name        = $request->route_name;
        $route->bus_id            = $request->bus_id;
        $route->terminal_start_id = $request->terminal_start_id;
        $route->terminal_end_id   = $request->terminal_end_id;
        $route->price             = $request->price;
        $route->active            = $request->active;

        if ($route->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Route successfully updated.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem updating this route. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($request)
    {
        $routesToDelete = $request->remove;

        foreach ( $routesToDelete as $index => $routeID ) {
            $route = Route::find($routeID);
            $route->delete();

            if ( $index == (count( $routesToDelete ) - 1) )
                return response()->json([
                    'success' => 1,
                    'message' => 'Route(s) successfully deleted.'
                ], 200);
        }
    }

    /**
     * @param $request
     * @return Route
     */
    private function createRouteStub($request)
    {
        $route = new Route();

        $route->route_name        = $request['route_name'];
        $route->bus_id            = $request['bus_id'];
        $route->terminal_start_id = $request['terminal_start_id'];
        $route->terminal_end_id   = $request['terminal_end_id'];
        $route->price             = $request['price'];
        $route->active            = $request['active'];

        return $route;
    }
}