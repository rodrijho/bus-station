<?php

namespace App\Repositories\Route;

/**
 * Interface RouteContract
 * @package App\Repositories\Route
 */
interface RouteContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllRoutes($order_by = 'id', $sort = 'asc');

    /**
     * @param $id
     * @return mixed
     */
    public function getRouteById($id);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $request
     * @return mixed
     */
    public function destroy($request);
}