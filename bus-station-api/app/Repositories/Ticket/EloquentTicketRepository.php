<?php

namespace App\Repositories\Ticket;

use App\Models\Ticket\Ticket;
use App\Models\Route\Route;
use App\Models\User;

/**
 * Class EloquentTicketRepository
 * @package App\Repositories\Ticket
 */
class EloquentTicketRepository implements TicketContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllTickets($order_by = 'id', $sort = 'asc')
    {
        $tickets = Ticket::orderBy($order_by, $sort)->get();

        return response()->json([
            'success'   => 1,
            'tickets' => $tickets->load('users', 'routes')->toArray(),
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTicketById($id)
    {
        $ticket = Ticket::find($id);

        if (! is_null($ticket)) {
            return response()->json([
                'success' => 1,
                'ticket' => [
                    'id'         => $ticket->id,
                    'user_id'    => $ticket->user_id,
                    'user'       => $ticket->users->name,
                    'route_id'   => $ticket->route_id,
                    'route'      => $ticket->routes->terminal_starts->name . ' / ' . $ticket->routes->terminal_ends->name,
                    'code'       => $ticket->code,
                    'created_at' => $ticket->created_at->format('Y-m-d H:i:s'),
                    'updated_at' => $ticket->updated_at->format('Y-m-d H:i:s'),
                ],
            ], 200);
        } else {
            return response()->json([
                'message' => 'That ticket does not exist.'
            ], 500);
        }
    }

    public function search($request)
    {
        $routes = array();
        $prices = array();
        $tickets = $this->createTicketStub($request);

        if (count($tickets) > 1) {
            foreach ($tickets as $t) {
                $ticket = new Ticket();

                $ticket->user_id = $t['user_id'];
                $ticket->route_id = $t['route_id'];
                $ticket->code = $t['code'];

                $routes[] = $ticket->routes->terminal_starts->name . ' - ' . $ticket->routes->terminal_ends->name;
                $prices[] = $ticket->routes->price;
            }

            $route_text = "";
            $total = 0;

            foreach ($routes as $route) {
                $route_text .= $route . " / ";
            }

            foreach ($prices as $price) {
                $total += $price;
            }

            if ($ticket) {
                return response()->json([
                    'success' => 1,
                    'route'   => rtrim($route_text, " /"),
                    'total'   => $total,
                ], 200);
            } else {
                return response()->json([
                    'message' => 'There was a problem searching this ticket. Please try again.'
                ], 500);
            }
        } else {
            $ticket = new Ticket();

            $ticket->user_id = $tickets['user_id'];
            $ticket->route_id = $tickets['route_id'];
            $ticket->code = $tickets['code'];

            if ($ticket) {
                return response()->json([
                    'success' => 1,
                    'route'   => $ticket->routes->terminal_starts->name . ' - ' . $ticket->routes->terminal_ends->name,
                    'total'   => $ticket->routes->price,
                ], 200);
            } else {
                return response()->json([
                    'message' => 'There was a problem searching this ticket. Please try again.'
                ], 500);
            }
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($request)
    {
        $routes = array();
        $prices = array();
        $tickets = $this->createTicketStub($request);

        if (count($tickets) > 1) {
            foreach ($tickets as $t) {
                $ticket = new Ticket();

                $ticket->user_id = $t['user_id'];
                $ticket->route_id = $t['route_id'];
                $ticket->code = $t['code'];

                $saved = $ticket->save();

                $routes[] = $ticket->routes->terminal_starts->name . ' - ' . $ticket->routes->terminal_ends->name;
                $prices[] = $ticket->routes->price;
            }

            $route_text = "";
            $total = 0;

            foreach ($routes as $route) {
                $route_text .= $route . " / ";
            }

            foreach ($prices as $price) {
                $total += $price;
            }

            if ($saved) {
                return response()->json([
                    'success' => 1,
                    'route'   => rtrim($route_text, " /"),
                    'total'   => $total,
                    'message' => 'Ticket successfully created.',
                ], 200);
            } else {
                return response()->json([
                    'message' => 'There was a problem creating this ticket. Please try again.'
                ], 500);
            }
        } else {
            $ticket = new Ticket();

            $ticket->user_id = $tickets['user_id'];
            $ticket->route_id = $tickets['route_id'];
            $ticket->code = $tickets['code'];

            if ($ticket->save()) {
                return response()->json([
                    'success' => 1,
                    'route'   => $ticket->routes->terminal_starts->name . ' - ' . $ticket->routes->terminal_ends->name,
                    'total'   => $ticket->routes->price,
                    'message' => 'Ticket successfully created.',
                ], 200);
            } else {
                return response()->json([
                    'message' => 'There was a problem creating this ticket. Please try again.'
                ], 500);
            }
        }
    }

    /**
     * @param $id
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $request)
    {
        $ticket = Ticket::find($id);

        $ticket->user_id  = $request->user_id;
        $ticket->route_id = $request->route_id;
        $ticket->code     = $request->code;

        if ($ticket->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Ticket successfully updated.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem updating this ticket. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id);

        if ($ticket->delete()) {
            return response()->json([
                'success' => 1,
                'message' => 'Ticket successfully deleted.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem deleting this ticket. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return Ticket|array
     */
    private function createTicketStub($request)
    {
        $code = $this->createCode();
        $conditions = ['terminal_start_id' => $request['start'], 'terminal_end_id' => $request['end']];

        $user = User::where('email', $request['username'])->get()->first();
        $route = Route::where($conditions)->get()->first();
        $ticket = new Ticket();
        $array = [];

        if (count($route) > 0) {
            $ticket->user_id  = $user->id;
            $ticket->route_id = $route->id;
            $ticket->code     = $code;

            return $ticket;
        } else {
            $routes = Route::where('terminal_start_id', $request['start'])->get();

            foreach ($routes as $route_start) {
                $new_cond = ['terminal_start_id' => $route_start->terminal_end_id, 'terminal_end_id' => $request['end']];
                $final = Route::where($new_cond)->get()->first();

                if ($final) {
                    $final_list = [
                        'user_id' => $user->id,
                        'route_id' => $final->id,
                        'code' => $code
                    ];

                    $start_list = [
                        'user_id' => $user->id,
                        'route_id' => $final->id,
                        'code' => $code
                    ];

                    $start_cond = ['terminal_start_id' => $request['start'], 'terminal_end_id' => $final->terminal_start_id];
                    $start = Route::where($start_cond)->get()->first();

                    if ($start) {
                        $start_list = [
                            'user_id' => $user->id,
                            'route_id' => $start->id,
                            'code' => $code
                        ];
                    }

                    $mantequilla = array_push($array, $start_list, $final_list);
                }
            }

            return $array;
        }
    }

    /**
     * @return bool|string
     */
    public function createCode() {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 16)), 0, 16);
    }
}