<?php

namespace App\Repositories\Ticket;

/**
 * Interface TicketContract
 * @package App\Repositories\Ticket
 */
interface TicketContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllTickets($order_by = 'id', $sort = 'asc');

    /**
     * @param $id
     * @return mixed
     */
    public function getTicketById($id);

    /**
     * @param $request
     * @return mixed
     */
    public function search($request);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);
}