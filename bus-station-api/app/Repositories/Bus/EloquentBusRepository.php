<?php

namespace App\Repositories\Bus;

use App\Models\Bus\Bus;

/**
 * Class EloquentBusRepository
 * @package App\Repositories\Bus
 */
class EloquentBusRepository implements BusContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllBuses($order_by = 'id', $sort = 'asc')
    {
        $buses = Bus::orderBy($order_by, $sort)->get();

        return response()->json([
            'success'   => 1,
            'info' => $buses->toArray(),
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBusById($id)
    {
        $bus = Bus::find($id);

        if (! is_null($bus)) {
            return response()->json([
                'success'  => 1,
                'info' => [
                    'id'            => $bus->id,
                    'name'          => $bus->name,
                    'driver_name'   => $bus->driver_name,
                    'bus_model'     => $bus->bus_model,
                    'seats'         => $bus->seats,
                    'active'        => $bus->active,
                    'created_at'    => $bus->created_at->format('Y-m-d H:i:s'),
                    'updated_at'    => $bus->updated_at->format('Y-m-d H:i:s'),
                ],
            ], 200);
        } else {
            return response()->json([
                'message' => 'That bus does not exist.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($request)
    {
        $bus = $this->createBusStub($request);

        if ($bus->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Bus successfully created.',
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem creating this bus. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $id
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $request)
    {
        $bus = Bus::find($id);

        $bus->name          = $request->name;
        $bus->driver_name   = $request->driver_name;
        $bus->bus_model     = $request->bus_model;
        $bus->seats         = $request->seats;
        $bus->active        = $request->active;

        if ($bus->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Bus successfully updated.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem updating this bus. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($request)
    {
        $busesToDelete = $request->remove;

        foreach ( $busesToDelete as $index => $busID ) {
            $bus = Bus::find($busID);
            $bus->delete();

            if ( $index == (count( $busesToDelete ) - 1) )
                return response()->json([
                    'success' => 1,
                    'message' => 'Bus successfully deleted.'
                ], 200);
        }
    }

    /**
     * @param $request
     * @return Bus
     */
    private function createBusStub($request)
    {
        $bus = new Bus();

        $bus->name          = $request['name'];
        $bus->driver_name   = $request['driver_name'];
        $bus->bus_model     = $request['bus_model'];
        $bus->seats         = $request['seats'];
        $bus->active        = $request['active'];

        return $bus;
    }
}