<?php

namespace App\Repositories\Bus;

/**
 * Interface BusContract
 * @package App\Repositories\Bus
 */
interface BusContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllBuses($order_by = 'id', $sort = 'asc');

    /**
     * @param $id
     * @return mixed
     */
    public function getBusById($id);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $request
     * @return mixed
     */
    public function destroy($request);
}