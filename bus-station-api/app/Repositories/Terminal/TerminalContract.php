<?php

namespace App\Repositories\Terminal;

/**
 * Interface TerminalContract
 * @package App\Repositories\Terminal
 */
interface TerminalContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllTerminals($order_by = 'id', $sort = 'asc');

    /**
     * Filtered terminals to exclude the selected in Routes Module
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getFilteredTerminals($not_id, $order_by = 'id', $sort = 'asc');

    /**
     * @param $id
     * @return mixed
     */
    public function getTerminalById($id);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $request
     * @return mixed
     */
    public function destroy($request);
}