<?php

namespace App\Repositories\Terminal;

use App\Models\Terminal\Terminal;

/**
 * Class EloquentCustomerRepository
 * @package App\Repositories\Customer
 */
class EloquentTerminalRepository implements TerminalContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllTerminals($order_by = 'id', $sort = 'asc')
    {
        $terminals = Terminal::orderBy($order_by, $sort)->get();

        return response()->json([
            'success'   => 1,
            'info' => $terminals->toArray(),
        ], 200);
    }

    /**
     * Filtered terminals to exclude the selected in Routes Module
     * @param string $order_by
     * @param string $sort
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFilteredTerminals($not_id, $order_by = 'id', $sort = 'asc')
    {
        $terminals = Terminal::orderBy($order_by, $sort)
        ->whereNotIn('id', [$not_id])
        ->get();

        return response()->json([
            'success'   => 1,
            'info' => $terminals->toArray(),
        ], 200);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTerminalById($id)
    {
        $terminal = Terminal::find($id);

        if (! is_null($terminal)) {
            return response()->json([
                'success'  => 1,
                'info' => [
                    'id'          => $terminal->id,
                    'name'        => $terminal->name,
                    'description' => $terminal->description,
                    'active'      => $terminal->active,
                    'created_at'  => $terminal->created_at->format('Y-m-d H:i:s'),
                    'updated_at'  => $terminal->updated_at->format('Y-m-d H:i:s'),
                ],
            ], 200);
        } else {
            return response()->json([
                'message' => 'That terminal does not exist.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($request)
    {
        $terminal = $this->createTerminalStub($request);

        if ($terminal->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Terminal successfully created.',
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem creating this terminal. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $id
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $request)
    {
        $terminal = Terminal::find($id);

        $terminal->name        = $request->name;
        $terminal->description = $request->description;
        $terminal->active      = $request->active;

        if ($terminal->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'Terminal successfully updated.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem updating this terminal. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($request)
    {
        $terminalToDelete = $request->remove;

        foreach ( $terminalToDelete as $index => $terminalID ) {
            $terminal = Terminal::find($terminalID);
            $terminal->delete();

            if ( $index == (count( $terminalToDelete ) - 1) )
                return response()->json([
                    'success' => 1,
                    'message' => 'Terminal successfully deleted.'
                ], 200);
        }
    }

    /**
     * @param $request
     * @return Terminal
     */
    private function createTerminalStub($request)
    {
        $terminal = new Terminal();

        $terminal->name        = $request['name'];
        $terminal->description = $request['description'];
        $terminal->active      = $request['active'];

        return $terminal;
    }
}