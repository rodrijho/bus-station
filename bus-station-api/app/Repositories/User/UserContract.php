<?php namespace App\Repositories\User;

/**
 * Interface UserContract
 * @package App\Repositories\User
 */
interface UserContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllUsers($order_by = 'id', $sort = 'asc');

    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $request
     * @return mixed
     */
    public function destroy($request);
}