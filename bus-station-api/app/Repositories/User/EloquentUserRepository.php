<?php namespace App\Repositories\User;

use App\Models\User;

/**
 * Class EloquentUserRepository
 * @package App\Repositories\User
 */
class EloquentUserRepository implements UserContract
{
    /**
     * @param string $order_by
     * @param string $sort
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUsers($order_by = 'id', $sort = 'asc')
    {
        $users = User::with('role')->orderBy($order_by, $sort)->get();

        $sanitizedUsers = [];
        $newUser = [];
        foreach( $users as $user ){
            $newUser =  [
                'id'         => $user->id,
                'name'       => $user->name,
                'email'      => $user->email,
                'role'       => ! empty( $user->role ) ?  $user->role->title : 'No Info',
                'created_at' => $user->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $user->updated_at->format('Y-m-d H:i:s'),
            ];

            array_push($sanitizedUsers, $newUser);
            $newUser = [];
        }

        return response()->json([
            'success'   => 1,
            'info' => $sanitizedUsers,
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserById($id)
    {
        $user = User::find($id);

        if (! is_null($user)) {
            return response()->json([
                'success'  => 1,
                'info' => [
                    'id'         => $user->id,
                    'name'       => $user->name,
                    'email'      => $user->email,
                    'role_id'    => $user->role_id,
                    'role'       => ! empty( $user->role ) ?  $user->role->title : 'No Info',
                    'created_at' => $user->created_at->format('Y-m-d H:i:s'),
                    'updated_at' => $user->updated_at->format('Y-m-d H:i:s'),
                ],
            ], 200);
        } else {
            return response()->json([
                'message' => 'The requested user does not exist.'
            ], 500);
        }
    }

    /**
     * Create a new user instance after a valid registration
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($request)
    {
        $user = $this->createUserStub($request);

        if ($user->save())
            return response()->json([
                'success' => 1,
                'message' => 'User successfully created.',
            ], 200);

        return response()->json([
            'message' => 'There was a problem creating this user. Please try again.'
        ], 500);
    }

    /**
     * @param $id
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $request)
    {
        $user = User::find($id);

        $user->name     = $request['name'];
        $user->email    = $request['email'];
        $user->role_id  = $request['role_id'];

        if ($user->save()) {
            return response()->json([
                'success' => 1,
                'message' => 'User successfully updated.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was a problem updating this User. Please try again.'
            ], 500);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($request)
    {
        $usersToDelete = $request->remove;

        foreach ( $usersToDelete as $index => $userID ) {
            $user = User::find($userID);
            $user->delete();

            if ( $index == (count( $usersToDelete ) - 1) )
                return response()->json([
                    'success' => 1,
                    'message' => 'User successfully deleted.'
                ], 200);
        }
    }

    /**
     * @param $request
     * @return User
     */
    private function createUserStub($request)
    {
        $user = new User();

        $user->name     = $request['name'];
        $user->email    = $request['email'];
        $user->role_id  = isset( $request['role_id'] ) ? $request['role_id'] : 2;
        $user->password = bcrypt($request['password']);

        return $user;
    }
}