<?php

namespace App\Http\Requests\Route;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Route
 */
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'route_name'        => 'required|string',
            'bus_id'            => 'required|integer',
            'terminal_start_id' => 'required|integer',
            'terminal_end_id'   => 'required|integer',
            'price'             => 'required',
            'active'            => 'required|bool',
        ];
    }
}