<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\User
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required', 'string' , 'max:50'],
            'email'     => ['required', 'string', 'email' , 'max:255', 'unique:users,email,'. $this->id],
            'role_id'   => ['required', 'integer'],
        ];
    }
}