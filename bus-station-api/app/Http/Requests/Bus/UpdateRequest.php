<?php

namespace App\Http\Requests\Bus;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Bus
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|max:50',
            'driver_name'   => 'required|string|max:191',
            'bus_model'     => 'required|string|max:191',
            'seats'         => 'required|integer',
            'active'        => 'required|bool',
        ];
    }
}