<?php namespace App\Http\Controllers\User;

use App\Models\Route\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserContract;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;

Class UserController extends Controller
{
    /**
     * @var $user
     */
    protected $user;

    /**
     * UserContract constructor.
     * @param UserContract $user
     */
    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->user->getAllUsers();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->user->getUserById($id);
    }

    /**
     * @param StoreUserRequest $request
     * @return Route
     */
    public function store(StoreUserRequest $request)
    {
        return $this->user->create($request);
    }

    /**
     * @param UpdateUserRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateUserRequest $request, $id)
    {
        return $this->user->update($id, $request);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function destroy( Request $request)
    {
        return $this->user->destroy($request);
    }
}