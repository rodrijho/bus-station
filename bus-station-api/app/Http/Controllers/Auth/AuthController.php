<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserRequest;
use App\Repositories\User\UserContract;

class AuthController extends Controller
{
    /**
     * @var UserContract
     */
    protected $user;

    /**
     * BusController constructor.
     * @param UserContract $user
     */
    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    /**
     * Create a new user instance after a valid registration
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {
        return $this->user->create($request);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->user->getUserById($id);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }
}
