<?php namespace App\Http\Controllers\Station\Route;

use App\Http\Controllers\Controller;
use App\Models\Route\Route;
use App\Http\Requests\Route\StoreRequest;
use App\Http\Requests\Route\UpdateRequest;
use App\Repositories\Route\RouteContract;
use Illuminate\Http\Request;

Class RouteController extends Controller
{
    /**
     * @var $route
     */
    protected $route;

    /**
     * RouteController constructor.
     * @param RouteContract $route
     */
    public function __construct(RouteContract $route)
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->route->getAllRoutes();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->route->getRouteById($id);
    }

    /**
     * @param StoreRequest $request
     * @return Route
     */
    public function store(StoreRequest $request)
    {
        return $this->route->create($request);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateRequest $request, $id)
    {
        return $this->route->update($id, $request);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function destroy( Request $request)
    {
        return $this->route->destroy($request);
    }
}