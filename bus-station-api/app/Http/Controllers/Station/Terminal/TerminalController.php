<?php

namespace App\Http\Controllers\Station\Terminal;

use App\Http\Controllers\Controller;
use App\Models\Terminal\Terminal;
use App\Http\Requests\Terminal\StoreRequest;
use App\Http\Requests\Terminal\UpdateRequest;
use App\Repositories\Terminal\TerminalContract;
use Illuminate\Http\Request;

Class TerminalController extends Controller
{
    /**
     * @var $terminal
     */
    protected $terminal;

    /**
     * TerminalController constructor.
     * @param TerminalContract $terminal
     */
    public function __construct(TerminalContract $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->terminal->getAllTerminals();
    }

    /**
     * Filtered terminals to exclude the selected in Routes Module
     * @return mixed
     */
    public function filterTerminals($not_ind)
    {
        return $this->terminal->getFilteredTerminals($not_ind);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->terminal->getTerminalById($id);
    }

    /**
     * @param StoreRequest $request
     * @return Terminal
     */
    public function store(StoreRequest $request)
    {
        return $this->terminal->create($request);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateRequest $request, $id)
    {
        return $this->terminal->update($id, $request);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        return $this->terminal->destroy($request);
    }
}