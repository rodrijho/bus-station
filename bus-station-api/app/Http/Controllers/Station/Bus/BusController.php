<?php

namespace App\Http\Controllers\Station\Bus;

use App\Http\Controllers\Controller;
use App\Models\Bus\Bus;
use App\Http\Requests\Bus\StoreRequest;
use App\Http\Requests\Bus\UpdateRequest;
use App\Repositories\Bus\BusContract;
use Illuminate\Http\Request;

Class BusController extends Controller
{
    /**
     * @var $bus
     */
    protected $bus;

    /**
     * BusController constructor.
     * @param BusContract $bus
     */
    public function __construct(BusContract $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->bus->getAllBuses();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->bus->getBusById($id);
    }

    /**
     * @param StoreRequest $request
     * @return Bus
     */
    public function store(StoreRequest $request)
    {
        return $this->bus->create($request);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateRequest $request, $id)
    {
        return $this->bus->update($id, $request);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy(Request $request)
    {
        return $this->bus->destroy($request);
    }
}