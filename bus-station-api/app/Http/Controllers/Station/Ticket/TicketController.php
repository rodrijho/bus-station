<?php

namespace App\Http\Controllers\Station\Ticket;

use App\Http\Controllers\Controller;
use App\Models\Ticket\Ticket;
use App\Http\Requests\Ticket\StoreRequest;
use App\Http\Requests\Ticket\UpdateRequest;
use App\Repositories\Ticket\TicketContract;

Class TicketController extends Controller
{
    /**
     * @var $route
     */
    protected $ticket;

    /**
     * TicketController constructor.
     * @param TicketContract $ticket
     */
    public function __construct(TicketContract $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->ticket->getAllTickets();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->ticket->getTicketById($id);
    }

    /**
     * @param StoreRequest $request
     * @return Ticket
     */
    public function search(StoreRequest $request)
    {
        return $this->ticket->search($request);
    }

    /**
     * @param StoreRequest $request
     * @return Ticket
     */
    public function store(StoreRequest $request)
    {
        return $this->ticket->create($request);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateRequest $request, $id)
    {
        return $this->ticket->update($id, $request);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->ticket->destroy($id);
    }
}