<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['cors'])->post('user/register', 'Auth\AuthController@store');

Route::middleware(['auth:api', 'cors'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/auth/refresh', function (Request $request) {
    Route::get('auth/refresh', 'Auth\AuthController@refresh');

});

Route::group(['middleware' => ['auth:api', 'cors']], function () {
    Route::group(['namespace' => 'User'], function () {
        Route::get('users/index', 'UserController@index');
        Route::get('users/{id}', 'UserController@show');
        Route::post('users', 'UserController@store');
        Route::put('users/{id}', 'UserController@update');
        Route::post('users/delete', 'UserController@destroy');
    });

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('auth/user/{id}', 'AuthController@show');
        Route::get('auth/refresh', 'AuthController@refresh');
    });

    Route::group(['namespace' => 'Station\Terminal'], function () {
        Route::get('terminal', 'TerminalController@index');
        Route::get('terminal/filtered/{not_in}', 'TerminalController@filterTerminals');
        Route::get('terminal/{id}', 'TerminalController@show');
        Route::post('terminal', 'TerminalController@store');
        Route::put('terminal/{id}', 'TerminalController@update');
        Route::post('terminal/delete', 'TerminalController@destroy');
    });

    Route::group(['namespace' => 'Station\Bus'], function () {
        Route::get('bus', 'BusController@index');
        Route::get('bus/{id}', 'BusController@show');
        Route::post('bus', 'BusController@store');
        Route::put('bus/{id}', 'BusController@update');
        Route::post('bus/delete', 'BusController@destroy');
    });

    Route::group(['namespace' => 'Station\Route'], function () {
        Route::get('route', 'RouteController@index');
        Route::get('route/{id}', 'RouteController@show');
        Route::post('route', 'RouteController@store');
        Route::put('route/{id}', 'RouteController@update');
        Route::post('route/delete', 'RouteController@destroy');
    });

    Route::group(['namespace' => 'Station\Ticket'], function () {
        Route::get('ticket', 'TicketController@index');
        Route::get('ticket/{id}', 'TicketController@show');
        Route::post('ticket', 'TicketController@store');
        Route::post('ticket/search', 'TicketController@search');
        Route::put('ticket/{id}', 'TicketController@update');
        Route::delete('ticket/{id}', 'TicketController@destroy');
    });
});
