<?php

return [
    'table_names' => [
        'users' => 'users',
        'terminals' => 'terminals',
        'buses' => 'buses',
        'routes' => 'routes',
        'tickets' => 'tickets',
    ],
];