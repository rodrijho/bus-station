<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routes extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create(config('station.table_names.routes'), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bus_id')->unsigned();
            $table->foreign('bus_id')->references('id')->on(config('station.table_names.buses'))->onDelete('cascade');
            $table->integer('terminal_start_id')->unsigned();
            $table->foreign('terminal_start_id')->references('id')->on(config('station.table_names.terminals'))->onDelete('cascade');
            $table->integer('terminal_end_id')->unsigned();
            $table->foreign('terminal_end_id')->references('id')->on(config('station.table_names.terminals'))->onDelete('cascade');
            $table->double('price');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('station.table_names.routes'));
    }
}
