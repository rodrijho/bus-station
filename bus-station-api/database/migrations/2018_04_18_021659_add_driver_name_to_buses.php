<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverNameToBuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(config('station.table_names.buses'), function($table) {
            $table->dropColumn('code');
            $table->string('driver_name', 100)->after('name');
            $table->string('bus_model', 100)->after('driver_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('station.table_names.buses'), function($table) {
            $table->dropColumn('driver_name');
            $table->dropColumn('bus_model');
        });
    }
}
