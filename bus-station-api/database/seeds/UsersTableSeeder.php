<?php use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('1234'),
                'role_id' => 1,
                'remember_token' => ''
            ],

        ];

        foreach ($items as $item) {
            \App\Models\User::create($item);
        }
    }
}
