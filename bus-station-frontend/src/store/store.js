import Vue from 'vue';
import Vuex from 'vuex';
import CreatePersistedState from 'vuex-persistedstate';
import { CLIENT_SECRET, CLIENT_ID, GRANT_TYPE } from "../../src/config/site";
import axios from "axios";
import qs from "qs";
Vue.use(Vuex);

let vueScope = null;

const nestedParams = {
  grant_type: GRANT_TYPE,
  client_id: CLIENT_ID,
  client_secret: CLIENT_SECRET
};


export const Store = new Vuex.Store({
  plugins: [CreatePersistedState()],

  state:{
    tokens: {
      access_token: null,
      expires_in: null,
      refresh_token: null,
      token_type: null,
    },
    currentUser:{
      name:null,
      email:null
    },
    moduleData:{}
  },

  getters: {
    isAuthenticated(state) {
      if (state.tokens.access_token){
        return true;
      }

      return false;
    },
    getToken(state) {
      return state.tokens.access_token;
    },
    getModuleData(state) {
      return state.moduleData;
    },
    isAdmin(state){
      if (state.currentUser.role_id != null && state.currentUser.role_id == 1){
        return true;
      }

      return false;
    }
  },

  actions:{
    login : (context, data) => {
      return new Promise( (resolve, reject) => {
        vueScope = data.vueInstance;
        vueScope.sendingRequest = true;
        let nestedData = Object.assign(nestedParams, data.user);;

        axios
          .post('oauth/token', qs.stringify(nestedData))
          .then(response => {
            context.commit('setToken', response.data);
            axios.defaults.headers.common["Authorization"] = "Bearer " + response.data.access_token;

            axios.get('api/user')
            .then(response => {
              context.commit('setCurrentUser', response.data);
              let information = {
                type: 'success',
                response: response,
                initialMessage: "Welcome Back " + response.data.name,
              };
              context.dispatch("handleNotifications", information);
              vueScope.$router.push({ name: data.redirectTo });
              resolve(response);
            })
            .catch(errorResponse => {
              let information = {
                type: 'error',
                response: errorResponse,
                initialMessage: "Something Went Wrong."
              };

              Store.dispatch("handleNotifications", information);

              reject(errorResponse);
            });
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });

      })
    },
    register : (context, data) => {
      return new Promise( (resolve, reject) => {
        vueScope = data.vueInstance;
        vueScope.sendingRequest = true;
        let nestedData = Object.assign(nestedParams, data.user);;

        axios
          .post('api/user/register', qs.stringify(nestedData))
          .then(response => {
            let information = {
              type: 'success',
              response: response,
              initialMessage: "Great News : " + response.data.message,
            };
            context.dispatch("handleNotifications", information);
            resolve(response);
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });

      })
    },
    logout: (context, data) => {
      vueScope = data.vueInstance;
      vueScope.sendingRequest = true;

      let information = {
        type: 'success',
        response: '',
        initialMessage: "Logged Out Successfully."
      };

      context.commit('destroyToken');
      context.commit('destroyCurrentUser');
      Store.dispatch("handleNotifications", information);
      vueScope.$router.push({ name: 'login' });
    },
    handleNotifications : (context, payload) => {
      let type = payload.type;
      let serverResponse = payload.response;
      let notificationMessage = payload.initialMessage;

      if (serverResponse.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (serverResponse.response.status == 422) {
          vueScope.error = true;
          vueScope.errors = serverResponse.response.data.errors;
        }
        notificationMessage =
          notificationMessage + " : " + serverResponse.response.data.message;
      } else if (serverResponse.request) {
        if(serverResponse.message)
        notificationMessage =
          notificationMessage + " : " + serverResponse.message;
      } else {
        if(serverResponse.message)
        notificationMessage =
          notificationMessage + " : " + serverResponse.message;
      }
      if (type == "error") {
        vueScope.success = false;
        vueScope.sendingRequest = false;
        Vue.toasted.error(notificationMessage, {
          //theme of the toast you prefer
          theme: "bubble",
          //position of the toast container
          position: "bottom-center",
          //display time of the toast
          duration: 5000
        });
        return;
      } else {
        vueScope.success = true;
        vueScope.sendingRequest = false;
        Vue.toasted.success(notificationMessage, {
          //theme of the toast you prefer
          theme: "bubble",
          //position of the toast container
          position: "bottom-center",
          //display time of the toast
          duration: 5000
        });
        return;
      }
    },
    async readOne ( context, payload ) {
      return new Promise( (resolve, reject) => {
        vueScope = payload.vueInstance;
        let nestedData = Object.assign(nestedParams);
        let lol = payload.fill;
        axios
          .get(payload.url)
          .then(response => {
            context.commit('setModuleData', response.data.info);
            vueScope.isLoading = false;
            resolve(response);
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });
      })
    },
    readAll: ( context, payload ) => {
      return new Promise( (resolve, reject) => {
        vueScope = payload.vueInstance;
        let nestedData = Object.assign(nestedParams);
        let action = 'show';

        let information = {
          action : action,
          vueInstance: vueScope
        };

        Store.dispatch("loadingModal", information);

        axios
          .get(payload.url)
          .then(response => {
            vueScope.rows = response.data.info;

            action = 'hide';
            information = {
              action : action,
              vueInstance: vueScope
            };
            Store.dispatch("loadingModal", information);
            resolve(response);
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });
      })
    },
    createRecord: ( context, payload ) => {
      return new Promise( (resolve, reject) => {
        vueScope = payload.vueInstance;
        let nestedData = Object.assign(payload.formData);

        axios
        .post(payload.url, qs.stringify(nestedData))
          .then(response => {

            let information = {
              type: 'success',
              response: response,
              initialMessage: "Great News " + response.data.message,
            };

            context.dispatch("handleNotifications", information);
            vueScope.$router.push({ name: payload.redirectTo });
            resolve(response);
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });
      })
    },
    updateRecord: ( context, payload ) => {
      return new Promise( (resolve, reject) => {
        vueScope = payload.vueInstance;
        let nestedData = Object.assign(payload.formData);

        axios
        .put(payload.url, qs.stringify(nestedData))
          .then(response => {

            let information = {
              type: 'success',
              response: response,
              initialMessage: "Great News " + response.data.message,
            };

            context.dispatch("handleNotifications", information);
            vueScope.$router.push({ name: payload.redirectTo });
            resolve(response);
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });
      })
    },
    deleteRecord: ( context, payload ) => {
      return new Promise( (resolve, reject) => {
        vueScope = payload.vueInstance;
        let nestedData = Object.assign(payload.formData);

        axios
        .post(payload.url, qs.stringify(nestedData))
          .then(response => {

            let information = {
              type: 'success',
              response: response,
              initialMessage: "Great News: " + response.data.message,
            };

            location.reload();
            context.dispatch("handleNotifications", information);
            resolve(response);
          })
          .catch(errorResponse => {
            let information = {
              type: 'error',
              response: errorResponse,
              initialMessage: "Something Went Wrong."
            };

            Store.dispatch("handleNotifications", information);

            reject(errorResponse);
          });
      })
    },
    loadingModal(context, payload) {
      if (payload.action == 'show') {
        payload.vueInstance.$swal.showLoading({text: 'Loading Data'})
      }else{
        payload.vueInstance.$swal.close();
      }
    },
  },
  mutations: {
    setModuleData: (state, data)  => {
      state.moduleData = data;
      state.moduleDataLoaded = true;
    },
    setCurrentUser: (state, data)  => {
      state.currentUser = data;
    },
    setToken : (state, token) => {
        state.tokens = token;
    },
    destroyToken: (state ) => {
      const emptyToken = {
        access_token: null,
        expires_in: null,
        refresh_token: null,
        token_type: null,
      };

      state.tokens = emptyToken;
    },
    destroyCurrentUser: (state ) => {
      const emptyCurrentUser = {
        name: null,
        email: null,
      };

      state.currentUser = emptyCurrentUser;
    },
    removeRowFromGrid: (state, rows) => {
      rows.remove.forEach(selectedRow => {
        let index = vueScope.rows.forEach( (record, index) => {
          if (record.id  == selectedRow) {
            delete vueScope.rows[index];
          }
        });

      });
    }
  }
});

