// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueRouter from "vue-router"
import axios from 'axios'
import VueAxios from 'vue-axios'
import QueryString from 'qs'
import Toasted from 'vue-toasted'
import VueGoodTable from 'vue-good-table'
import {Store} from '../src/store/store'
import VueSweetAlert from 'vue-sweetalert'
import {API_URL} from './config/site'

Vue.use(VueRouter);
Vue.use(QueryString);
Vue.use(VueAxios, axios);
Vue.use(Toasted);
Vue.use(VueGoodTable);
Vue.use(VueSweetAlert);

axios.defaults.baseURL = API_URL;
axios.defaults.crossDomain = true;
axios.defaults.headers.common["Accept"] = "application/json";
axios.defaults.headers.common["Content-Type"] = "application/x-www-form-urlencoded";
axios.defaults.headers.common["Authorization"] = "Bearer " + Store.state.tokens.access_token;

window.axios = axios;
window.Store = Store;

Vue.config.productionTip = false

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {

  if (error.status == 404) {
    Vue.toasted.error(error.request.body.error, {
      //theme of the toast you prefer
      theme: "bubble",
      //position of the toast container
      position: "bottom-center",
      //display time of the toast
      duration: 5000
    });
  } else if (error.request.status == 500) {
    window.history.back();
    Vue.toasted.error('We are experiencing a problem in our servers!', {
      //theme of the toast you prefer
      theme: "bubble",
      //position of the toast container
      position: "bottom-center",
      //display time of the toast
      duration: 5000
    });
  } else if (error.request.status == 401) {
    Store.commit('destroyToken');
    Store.commit('destroyCurrentUser');
    router.push({name: 'login'});
    location.reload();
  }
  return Promise.reject(error);
});

let referer = '';
router.beforeEach(
  (to, from, next) => {
      referer = from;
      if (to.matched.some(record => record.meta.auth)) {
        // If the user isn't authenticated we must show the login form
        if (! Store.getters.isAuthenticated ) {
            next({
                path: '/login',
            });

            Vue.toasted.error('Please login to navigate in the application !', {
              //theme of the toast you prefer
              theme: "bubble",
              //position of the toast container
              position: "bottom-center",
              //display time of the toast
              duration: 5000
            });
        } else next()

        // If the user isn't an administrator then they cannot enter to the module
        if ( to.meta.admin && ! Store.getters.isAdmin ) {
          next({
                path: '/home',
          });
      
          Vue.toasted.error('Sorry this module is only for administrator users !', {
            //theme of the toast you prefer
            theme: "bubble",
            //position of the toast container
            position: "bottom-center",
            //display time of the toast
            duration: 5000
          });
        } else next()
    } else next()
  }
);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
