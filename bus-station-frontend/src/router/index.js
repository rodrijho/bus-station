import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from "@/components/access/Login"
import Register from "@/components/access/Register"

import BusesIndex from "@/components/management/buses/Index"
import BusesCreate from "@/components/management/buses/Create"
import BusesEdit from "@/components/management/buses/Edit"

import TerminalIndex from "@/components/management/terminal/Index"
import TerminalCreate from "@/components/management/terminal/Create"
import TerminalEdit from "@/components/management/terminal/Edit"

import RouteIndex from "@/components/management/routes/Index"
import RouteCreate from "@/components/management/routes/Create"
import RouteEdit from "@/components/management/routes/Edit"

import UserIndex from "@/components/management/users/Index"
import UserCreate from "@/components/management/users/Create"
import UserEdit from "@/components/management/users/Edit"

Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      redirect: "home"
    },
    {
      path: "/home",
      name: "home",
      component: Home,
      meta: {
        forVisitors: true
    }
    },
    ,
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        forVisitors: true
      }
    },
    ,
    {
      path: "/register",
      name: "register",
      component: Register,
      meta: {
        forVisitors: true
      }
    },
    {
      path: "/management/users/index",
      name: "users.index",
      component: UserIndex,
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/users/create",
      name: "users.create",
      component: UserCreate,
      meta: {
        auth: true,
        admin: true,
      }
    },
    { path: '/management/users/edit/:id',
      component: UserEdit,
      name: 'users.edit',
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/buses/index",
      name: "buses.index",
      component: BusesIndex,
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/buses/create",
      name: "buses.create",
      component: BusesCreate,
      meta: {
        auth: true,
        admin: true,
      }
    },
    { path: '/management/buses/edit/:id',
      component: BusesEdit,
      name: 'buses.edit',
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/terminal/index",
      name: "terminal.index",
      component: TerminalIndex,
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/terminal/create",
      name: "terminal.create",
      component: TerminalCreate,
      meta: {
        auth: true,
        admin: true,
      }
    },
    { path: '/management/terminal/edit/:id',
      component: TerminalEdit,
      name: 'terminal.edit',
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/routes/index",
      name: "routes.index",
      component: RouteIndex,
      meta: {
        auth: true,
        admin: true,
      }
    },
    {
      path: "/management/routes/create",
      name: "routes.create",
      component: RouteCreate,
      meta: {
        auth: true,
        admin: true,
      }
    },
    { path: '/management/routes/edit/:id',
      component: RouteEdit,
      name: 'routes.edit',
      meta: {
        auth: true,
        admin: true,
      }
    }

  ],
  linkActiveClass: "active", // active class for non-exact links.
  linkExactActiveClass: "active" // active class for *exact* links.
})
