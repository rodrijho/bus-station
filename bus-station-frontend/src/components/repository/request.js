import { CLIENT_SECRET, CLIENT_ID, GRANT_TYPE } from "../../config/site";

import axios from "axios";
import qs from "qs";
import Vue from "vue";

let vueScope = null;

const nestedParams = {
  grant_type: GRANT_TYPE,
  client_id: CLIENT_ID,
  client_secret: CLIENT_SECRET
};

export default {
  methods: {
    login(url, params, vueInstance, redirectTo) {
      vueScope = vueInstance;
      vueScope.sendingRequest = true;
      let nestedData = this.assignFormParams(params);

      axios
        .post(url, qs.stringify(nestedData))
        .then(response => {
          console.log(response.data);
          return;
          this.handleNotifications("success", response, "Welcome Back ");
          vueScope.$router.push({ name: redirectTo });
        })
        .catch(errorResponse => {
          this.handleNotifications(
            "error",
            errorResponse,
            "Something Went Wrong."
          );
        });
    },
    assignFormParams(formParams) {
      return Object.assign(nestedParams, formParams);
    },
    create(url, params, vueInstance, redirectTo) {
      vueScope = vueInstance;
      vueScope.sendingRequest = true;
      let nestedData = this.assignFormParams(params);

      axios
        .post(url, qs.stringify(nestedData))
        .then(response => {
          this.handleNotifications("success", response, "Great News.");
          if (redirectTo) vueScope.$router.push({ name: redirectTo });
        })
        .catch(errorResponse => {
          this.handleNotifications(
            "error",
            errorResponse,
            "Something Went Wrong."
          );
        });
    },
    read(url, params, vueInstance, objectToLoad) {
      axios
        .get(url + params)
        .then(function(resp) {
          objectToLoad = resp.data;
        })
        .catch(errorResponse => {
          this.handleNotifications(
            "error",
            errorResponse,
            "Something Went Wrong."
          );
        });
    },
    update(url, params, vueInstance, redirectTo) {
      let nestedData = this.assignFormParams(params);
      axios
        .patch(url, qs.stringify(nestedData))
        .then(response => {
          this.handleNotifications("success", response, "Great News.");
          vueScope.$router.push({ name: redirectTo });
        })
        .catch(errorResponse => {
          this.handleNotifications(
            "error",
            errorResponse,
            "Something Went Wrong."
          );
        });
    },
    del(id) {
      // To do
    },
    getAll(url, params, vueInstance) {
      alert("GETTING ALL");
      let nestedData = this.assignFormParams(params);
      axios
        .get(url, qs.stringify(nestedData))
        .then(response => {
          console.log(response);
          return;
          vueInstance.rows = response.data;
        })
        .catch(errorResponse => {
          this.handleNotifications(
            "error",
            errorResponse,
            "Something Went Wrong."
          );
        });
    },
    handleNotifications(type, serverResponse, notificationMessage) {
      if (serverResponse.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (serverResponse.response.status == 422) {
          vueScope.error = true;
          vueScope.errors = serverResponse.response.data.errors;
        }
        notificationMessage =
          notificationMessage + " : " + serverResponse.response.data.message;

        console.log(serverResponse.response.data);
        console.log(serverResponse.response.status);
        console.log(serverResponse.response.headers);
      } else if (serverResponse.request) {
        // Server Responses
        notificationMessage =
          notificationMessage + " : " + serverResponse.data.message;
      } else {
        notificationMessage =
          notificationMessage + " : " + serverResponse.message;
      }
      if (type == "error") {
        vueScope.success = false;

        Vue.toasted.error(notificationMessage, {
          //theme of the toast you prefer
          theme: "bubble",
          //position of the toast container
          position: "bottom-center",
          //display time of the toast
          duration: 5000
        });
      } else {
        vueScope.success = true;
        vueScope.sendingRequest = false;
        Vue.toasted.success(notificationMessage, {
          //theme of the toast you prefer
          theme: "bubble",
          //position of the toast container
          position: "bottom-center",
          //display time of the toast
          duration: 5000
        });
      }
    }
  }
};
